package com.prueba.mastermind.jugadas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.mastermind.exceptions.ServiceException;
import com.prueba.mastermind.jugadas.dto.JugadaDto;
import com.prueba.mastermind.jugadas.dto.JugadaResponseDto;
import com.prueba.mastermind.jugadas.servicio.JugadasService;
import com.prueba.mastermind.utils.HttpResponsesJugadasUtil;

/**
 * The Class JugadasController.
 */
@RestController
@RequestMapping(value = "/jugadas", produces = "application/json")
@CrossOrigin(origins = "http://localhost:4200")
public class JugadasController {

	/** The servicio jugadas. */
	@Autowired
	private JugadasService servicioJugadas;
	
	/**
	 * Jugar.
	 *
	 * @param jugada the jugada
	 * @return the response entity
	 */
	@RequestMapping(value = {"/jugar"}, method = RequestMethod.POST)
	public ResponseEntity<JugadaResponseDto> jugar(@RequestBody JugadaDto jugada) {
		try {
			JugadaDto jugadaRespuesta = servicioJugadas.jugar(jugada);
			return HttpResponsesJugadasUtil.encapsularRespuestaSimple(jugadaRespuesta, jugadaRespuesta.getError());
		} catch(ServiceException se) {
			return HttpResponsesJugadasUtil.encapsularRespuestaSimple(null, se.getMessage());
		}
	}
	
	/**
	 * Gets the jugadas.
	 *
	 * @param id the id
	 * @return the jugadas
	 */
	@RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
	public ResponseEntity<JugadaResponseDto> getJugadas(@RequestParam(value = "id", required = false) Long id) {
		try {
			List<JugadaDto> listJugadas = servicioJugadas.obtenerJugadas(id);
			return HttpResponsesJugadasUtil.encapsularRespuestaLista(listJugadas, null);
		} catch(ServiceException se) {
			return HttpResponsesJugadasUtil.encapsularRespuestaSimple(null, se.getMessage());
		}
	}
	
	/**
	 * Gets the partida.
	 *
	 * @param id the id
	 * @return the partida
	 */
	@RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
	public ResponseEntity<JugadaResponseDto> getPartida(@PathVariable Long id) {
		try {
			JugadaDto jugadaExistente = servicioJugadas.obtenerJugada(id);
			return HttpResponsesJugadasUtil.encapsularRespuestaSimple(jugadaExistente, null);
		} catch(ServiceException se) {
			return HttpResponsesJugadasUtil.encapsularRespuestaSimple(null, se.getMessage());
		}
	}
}
