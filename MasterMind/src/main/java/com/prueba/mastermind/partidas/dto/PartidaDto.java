/*
 * 
 */
package com.prueba.mastermind.partidas.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prueba.mastermind.jugadas.dto.JugadaDto;
import com.prueba.mastermind.utils.DateSerializerUtil;

/**
 * The Class PartidaDto.
 * Representa una partida
 */
@Entity
@Table(name = "partidas")
public class PartidaDto {
	
	/** The id. */
	private Long id;

	/** The nombre. */
	private String nombre;

	/** The fec creacion. */
	@JsonSerialize(using = DateSerializerUtil.class)
	private ZonedDateTime fecCreacion;
	
	/** The codigo correcto. 
	 *  No se devuelve el codigo correcto a la salida del WebService con la etiqueta Ignore
	 */
	@JsonIgnore
	private String codigoCorrecto;
	
	/** The estado. */
	private Integer estado;
	
	private Integer intentosRestantes = 10;
	
	/** The lst jugadas. */
	@JsonIgnore
	private List<JugadaDto> lstJugadas = new ArrayList<>();
	
	/** The error. */
	@JsonIgnore
	private String error;

	/**
	 * Instantiates a new partida dto.
	 *
	 * @param id the id
	 * @param nombre the nombre
	 * @param fecCreacion the fec creacion
	 * @param codigoCorrecto the codigo correcto
	 * @param estado the estado
	 * @param lstJugadas the lst jugadas
	 */
	public PartidaDto(Long id, String nombre, ZonedDateTime fecCreacion, String codigoCorrecto, Integer estado,
			List<JugadaDto> lstJugadas) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.fecCreacion = fecCreacion;
		this.codigoCorrecto = codigoCorrecto;
		this.estado = estado;
		this.lstJugadas = lstJugadas;
	}

	/**
	 * Instantiates a new partida dto.
	 *
	 * @param id the id
	 * @param nombre the nombre
	 * @param fecCreacion the fec creacion
	 * @param codigoCorrecto the codigo correcto
	 * @param estado the estado
	 */
	public PartidaDto(Long id, String nombre, ZonedDateTime fecCreacion, String codigoCorrecto, Integer estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.fecCreacion = fecCreacion;
		this.codigoCorrecto = codigoCorrecto;
		this.estado = estado;
	}

	/**
	 * Instantiates a new partida dto.
	 */
	public PartidaDto() {
		super();
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@Id
	@Column(name = "id_partida")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the fec creacion.
	 *
	 * @return the fec creacion
	 */
	@Column(name = "fec_creacion", insertable = false, updatable = false)
	public ZonedDateTime getFecCreacion() {
		return fecCreacion;
	}

	/**
	 * Sets the fec creacion.
	 *
	 * @param fecCreacion the new fec creacion
	 */
	public void setFecCreacion(ZonedDateTime fecCreacion) {
		this.fecCreacion = fecCreacion;
	}

	/**
	 * Gets the codigo correcto.
	 *
	 * @return the codigo correcto
	 */
	@Column(name = "codigo_correcto")
	public String getCodigoCorrecto() {
		return codigoCorrecto;
	}

	/**
	 * Sets the codigo correcto.
	 *
	 * @param codigoCorrecto the new codigo correcto
	 */
	public void setCodigoCorrecto(String codigoCorrecto) {
		this.codigoCorrecto = codigoCorrecto;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public Integer getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the new estado
	 */
	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	/**
	 * Gets the lst jugadas.
	 *
	 * @return the lst jugadas
	 */
	@Transient
	public List<JugadaDto> getLstJugadas() {
		return lstJugadas;
	}

	/**
	 * Sets the lst jugadas.
	 *
	 * @param lstJugadas the new lst jugadas
	 */
	public void setLstJugadas(List<JugadaDto> lstJugadas) {
		this.lstJugadas = lstJugadas;
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * Sets the error.
	 *
	 * @param error the new error
	 */
	public void setError(String error) {
		this.error = error;
	}

	@Column(name = "intentos")
	public Integer getIntentosRestantes() {
		return intentosRestantes;
	}

	public void setIntentosRestantes(Integer intentosRestantes) {
		this.intentosRestantes = intentosRestantes;
	}
}
