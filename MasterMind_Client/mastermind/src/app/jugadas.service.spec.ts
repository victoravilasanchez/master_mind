import { TestBed } from '@angular/core/testing';

import { JugadasService } from './jugadas.service';

describe('JugadasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JugadasService = TestBed.get(JugadasService);
    expect(service).toBeTruthy();
  });
});
