import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

const colorMap = {
  'O': 'orange',
  'R': 'red',
  'G': 'green',
  'B': 'blue',
  'Y': 'yellow',
  'P': 'pink',
  'W': 'white',
  'X': 'black'
};

const endpoints = {
  'PLAY_PLAY': 'http://localhost:8888/MasterMind/jugadas/jugar',
  'FECTH_PLAYS': 'http://localhost:8888/MasterMind/jugadas'
};

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class JugadasService {

  constructor(private http: HttpClient) { }

  jugar(jugada: string, id: number) {
    return this.http.post(endpoints.PLAY_PLAY, JSON.stringify({ 'partidaId': id , 'codigoJugado': jugada}), httpOptions).pipe(
      map((value: any) => {
        console.log(value);
        return value;
      })
    );
  }

  getJugadasFromPartida(id: number) {
    return this.http.get(endpoints.FECTH_PLAYS, { params: {
        id: id.toString()
      }
    }).pipe(
      map((value: any) => {
        const array = value.lstJugadas;
        const response = [];
        array.forEach(element => {
          const coloresJugados = element.codigoJugado.split(',');
          response.push(
            { id: element.id,
              jugada: { p1: colorMap[coloresJugados[0]], p2: colorMap[coloresJugados[1]]
              , p3: colorMap[coloresJugados[2]], p4: colorMap[coloresJugados[3]] },
              respuesta: this.devolverColores(element.resultadoJugado.split(','))
            }
          );
        });
        return response;
      })
    );
  }

  devolverColores(array: any) {
    const arrayColores = [];
    array.forEach(element => {
      arrayColores.push(colorMap[element]);
    });
    return arrayColores;
  }
}
