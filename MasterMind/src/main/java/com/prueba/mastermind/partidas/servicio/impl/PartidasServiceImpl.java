package com.prueba.mastermind.partidas.servicio.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import com.prueba.mastermind.conf.HibernateUtil;
import com.prueba.mastermind.exceptions.ServiceException;
import com.prueba.mastermind.partidas.constantes.PartidasConstantes;
import com.prueba.mastermind.partidas.dto.PartidaDto;
import com.prueba.mastermind.partidas.servicio.PartidasService;

/**
 * The Class PartidasServiceImpl.
 */
@Service("PartidasService")
public class PartidasServiceImpl implements PartidasService {

	/* (non-Javadoc)
	 * @see com.prueba.mastermind.partidas.servicio.PartidasService#obtenerPartidas()
	 */
	@Override
	public List<PartidaDto> obtenerPartidas() throws ServiceException {
		return HibernateUtil.selectFrom(PartidaDto.class, HibernateUtil.getSessionFactory().openSession());
	}

	/* (non-Javadoc)
	 * @see com.prueba.mastermind.partidas.servicio.PartidasService#obtenerPartida(java.lang.Long)
	 */
	@Override
	public PartidaDto obtenerPartida(Long id) throws ServiceException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		PartidaDto partidaDto = session.get(PartidaDto.class, id);
		
		return partidaDto;
	}

	/* (non-Javadoc)
	 * @see com.prueba.mastermind.partidas.servicio.PartidasService#comenzarPartida(java.lang.String)
	 */
	@Override
	public PartidaDto comenzarPartida(String name) throws ServiceException {
		PartidaDto partidaDto = new PartidaDto();
		partidaDto.setNombre(name);
		partidaDto.setCodigoCorrecto(generarCodigoCorrecto());
		partidaDto.setEstado(0);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Long id = (Long) session.save(partidaDto);
	    session.getTransaction().commit();
	    
	    session.close();
		
	    partidaDto.setId(id);
		return partidaDto;
	}
	
	/**
	 * Generar codigo correcto.
	 *
	 * @return the string
	 */
	private String generarCodigoCorrecto() {
		List<String> colores = PartidasConstantes.resultados;

		Collections.shuffle(colores);
		
		return StringUtils.join(colores.subList(0, 4), ",");
	}
}
