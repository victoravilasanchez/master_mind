package com.prueba.mastermind.utils;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.prueba.mastermind.partidas.dto.PartidaDto;
import com.prueba.mastermind.partidas.dto.PartidaResponseDto;

/**
 * The Class HttpResponsesPartidasUtil.
 */
public class HttpResponsesPartidasUtil {

	/**
	 * Encapsular respuesta simple.
	 *
	 * @param partida the partida
	 * @param textoError the texto error
	 * @return the response entity
	 */
	public static ResponseEntity<PartidaResponseDto> encapsularRespuestaSimple(PartidaDto partida, String textoError) {
		if(StringUtils.isBlank(textoError)) {
			if(partida != null) {
				return new ResponseEntity<PartidaResponseDto>(new PartidaResponseDto(partida), HttpStatus.OK);
			} else {
				return new ResponseEntity<PartidaResponseDto>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<PartidaResponseDto>(new PartidaResponseDto(null, textoError), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Encapsular respuesta lista.
	 *
	 * @param partidas the partidas
	 * @param textoError the texto error
	 * @return the response entity
	 */
	public static ResponseEntity<PartidaResponseDto> encapsularRespuestaLista(List<PartidaDto> partidas, String textoError) {
		if(StringUtils.isBlank(textoError)) {
			if(partidas != null) {
				return new ResponseEntity<PartidaResponseDto>(new PartidaResponseDto(partidas), HttpStatus.OK);
			} else {
				return new ResponseEntity<PartidaResponseDto>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<PartidaResponseDto>(new PartidaResponseDto(null, textoError), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
