package com.prueba.mastermind.partidas.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class PartidaResponseDto.
 */
public class PartidaResponseDto {
	
	/** The lst partidas. */
	private List<PartidaDto> lstPartidas = new ArrayList<PartidaDto>();
	
	/** The texto error. */
	private String textoError;

	/**
	 * Instantiates a new partida response dto.
	 *
	 * @param lstPartidas the lst partidas
	 * @param textoError the texto error
	 */
	public PartidaResponseDto(List<PartidaDto> lstPartidas, String textoError) {
		super();
		this.lstPartidas = lstPartidas;
		this.textoError = textoError;
	}

	/**
	 * Instantiates a new partida response dto.
	 *
	 * @param textoError the texto error
	 */
	public PartidaResponseDto(String textoError) {
		super();
		this.textoError = textoError;
	}

	/**
	 * Instantiates a new partida response dto.
	 *
	 * @param lstPartidas the lst partidas
	 */
	public PartidaResponseDto(List<PartidaDto> lstPartidas) {
		super();
		this.lstPartidas = lstPartidas;
	}
	
	
	/**
	 * Instantiates a new partida response dto.
	 *
	 * @param partida the partida
	 */
	public PartidaResponseDto(PartidaDto partida) {
		super();
		this.lstPartidas.add(partida);
	}

	/**
	 * Instantiates a new partida response dto.
	 */
	public PartidaResponseDto() {
		super();
	}

	/**
	 * Gets the texto error.
	 *
	 * @return the texto error
	 */
	public String getTextoError() {
		return textoError;
	}

	/**
	 * Sets the texto error.
	 *
	 * @param textoError the new texto error
	 */
	public void setTextoError(String textoError) {
		this.textoError = textoError;
	}

	/**
	 * Gets the lst partidas.
	 *
	 * @return the lst partidas
	 */
	public List<PartidaDto> getLstPartidas() {
		return lstPartidas;
	}

	/**
	 * Sets the lst partidas.
	 *
	 * @param lstPartidas the new lst partidas
	 */
	public void setLstPartidas(List<PartidaDto> lstPartidas) {
		this.lstPartidas = lstPartidas;
	}
}
