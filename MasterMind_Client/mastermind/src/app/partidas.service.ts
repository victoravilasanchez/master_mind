import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

const endpoints = {
  'START_GAME': 'http://localhost:8888/MasterMind/partidas/comenzarPartida',
  'FECTH_GAMES': 'http://localhost:8888/MasterMind/partidas'
};

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class PartidasService {

  constructor(private http: HttpClient) { }

  startGame(name: string) {
    return this.http.post(endpoints.START_GAME, name, httpOptions).pipe(
      map((value: any) => {
        const array = value.lstPartidas;
        const response = [];
        array.forEach(element => {
          response.push({ name: element.nombre, id: element.id, estado: element.estado });
        });
        return response;
      })
    );
  }

  getAllGames() {
    return this.http.get(endpoints.FECTH_GAMES).pipe(
      map((value: any) => {
        const array = value.lstPartidas;
        const response = [];
        array.forEach(element => {
          response.push({ name: element.nombre, id: element.id, estado: element.estado });
        });
        return response;
      })
    );
  }

  getGame(id: number) {
    return this.http.get(endpoints.FECTH_GAMES + '/' + id).pipe(
      map((value: any) => {
        const array = value.lstPartidas;
        const response = [];
        array.forEach(element => {
          response.push({ name: element.nombre, id: element.id, estado: element.estado, intentos: element.intentosRestantes });
        });
        return response;
      })
    );
  }

}
