import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PartidaDetailComponent } from './partida-detail/partida-detail.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'partidaDetail/:id', component: PartidaDetailComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
