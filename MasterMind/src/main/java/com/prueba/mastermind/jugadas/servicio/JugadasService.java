package com.prueba.mastermind.jugadas.servicio;

import java.util.List;

import com.prueba.mastermind.exceptions.ServiceException;
import com.prueba.mastermind.jugadas.dto.JugadaDto;

/**
 * The Interface JugadasService.
 */
public interface JugadasService {

	/**
	 * Obtener jugadas.
	 *
	 * @param id the id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<JugadaDto> obtenerJugadas(Long id) throws ServiceException;
	
	/**
	 * Obtener jugada.
	 *
	 * @param id the id
	 * @return the jugada dto
	 * @throws ServiceException the service exception
	 */
	public JugadaDto obtenerJugada(Long id) throws ServiceException;
	
	/**
	 * Jugar.
	 *
	 * @param jugada the jugada
	 * @return the jugada dto
	 * @throws ServiceException the service exception
	 */
	public JugadaDto jugar(JugadaDto jugada) throws ServiceException;
}
