package com.prueba.mastermind.utils;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * The Class CustomDateSerializer.
 * Esta clase se usa para formatear de manera correcta las fechas de los JSON que convierte jackson
 * de manera automatica. Usa un formato estandar YYYY-MM-DD HH:mi:ss
 */
public class DateSerializerUtil extends StdSerializer<ZonedDateTime> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7205002928369763870L;

	/**
	 * Instantiates a new custom date serializer.
	 */
	public DateSerializerUtil(){
        this(null);
    }
	
	/**
	 * Instantiates a new custom date serializer.
	 *
	 * @param t the t
	 */
	protected DateSerializerUtil(Class<ZonedDateTime> t) {
		super(t);
	}

	/** The formatter. */
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");

	/* (non-Javadoc)
	 * @see com.fasterxml.jackson.databind.ser.std.StdSerializer#serialize(java.lang.Object, com.fasterxml.jackson.core.JsonGenerator, com.fasterxml.jackson.databind.SerializerProvider)
	 */
	@Override
	public void serialize(ZonedDateTime value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		// Le anadimos al final de la fecha formateada el timezone del servidor, informacion relevante 
		gen.writeString(formatter.format(value));
	}
	
	
}
