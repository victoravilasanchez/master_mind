package com.prueba.mastermind.utils;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.prueba.mastermind.jugadas.dto.JugadaDto;
import com.prueba.mastermind.jugadas.dto.JugadaResponseDto;

/**
 * The Class HttpResponsesJugadasUtil.
 */
public class HttpResponsesJugadasUtil {
	
	/**
	 * Encapsular respuesta simple.
	 *
	 * @param jugada the jugada
	 * @param textoError the texto error
	 * @return the response entity
	 */
	public static ResponseEntity<JugadaResponseDto> encapsularRespuestaSimple(JugadaDto jugada, String textoError) {
		if(StringUtils.isBlank(textoError)) {
			return new ResponseEntity<JugadaResponseDto>(new JugadaResponseDto(jugada), HttpStatus.OK);
		} else {
			return new ResponseEntity<JugadaResponseDto>(new JugadaResponseDto(null, textoError), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Encapsular respuesta lista.
	 *
	 * @param jugadas the jugadas
	 * @param textoError the texto error
	 * @return the response entity
	 */
	public static ResponseEntity<JugadaResponseDto> encapsularRespuestaLista(List<JugadaDto> jugadas, String textoError) {
		if(StringUtils.isBlank(textoError)) {
			return new ResponseEntity<JugadaResponseDto>(new JugadaResponseDto(jugadas), HttpStatus.OK);
		} else {
			return new ResponseEntity<JugadaResponseDto>(new JugadaResponseDto(null, textoError), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
