package com.prueba.mastermind.conf;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * The Class HibernateUtil.
 */
public class HibernateUtil {
    
    /** The registry. */
    private static StandardServiceRegistry registry;
    
    /** The session factory. */
    private static SessionFactory sessionFactory;

    /**
     * Gets the current session.
     *
     * @return the current session
     */
    public static Session getCurrentSession() {
    	return getSessionFactory().getCurrentSession();
    }
    
    /**
     * Gets the session factory.
     *
     * @return the session factory
     */
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                // Create registry
                registry = new StandardServiceRegistryBuilder().configure().build();

                // Create MetadataSources
                MetadataSources sources = new MetadataSources(registry);

                // Create Metadata
                Metadata metadata = sources.getMetadataBuilder().build();

                // Create SessionFactory
                sessionFactory = metadata.getSessionFactoryBuilder().build();

            } catch (Exception e) {
                e.printStackTrace();
                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);
                }
            }
        }
        return sessionFactory;
    }

    /**
     * Shutdown.
     */
    public static void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }
    
    /**
     * Select from.
     *
     * @param <T> the generic type
     * @param type the type
     * @param session the session
     * @return the list
     */
    public static <T> List<T> selectFrom(Class<T> type, Session session) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<T> criteria = cb.createQuery(type);
        criteria.from(type);
        List<T> data = session.createQuery(criteria).getResultList();
        return data;
    }
    
    /**
     * Select from with ID. Funcion que se usa para seleccionar registros por ID en la 
     * tabla partidas desde la jugada(idPartida)
     *
     * @param <T> the generic type
     * @param type the type
     * @param session the session
     * @param id the id
     * @return the list
     */
    public static <T> List<T> selectFromWithID(Class<T> type, Session session, Long id) {
    	CriteriaBuilder cb = session.getCriteriaBuilder();
    	CriteriaQuery<T> q = cb.createQuery(type);
    	Root<T> c = q.from(type);
    	q.select(c);
    	ParameterExpression<Long> p = cb.parameter(Long.class);
    	q.where(cb.equal(c.get("partidaId"), p));
    	
    	TypedQuery<T> query = session.createQuery(q);
    	query.setParameter(p, id);
    	
    	List<T> data = query.getResultList();
    	 
        return data;
    }
}