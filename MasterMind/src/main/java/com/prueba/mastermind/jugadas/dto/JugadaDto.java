package com.prueba.mastermind.jugadas.dto;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prueba.mastermind.utils.DateSerializerUtil;

/**
 * The Class JugadaDto.
 * Representa una jugada
 */
@Entity
@Table(name = "jugadas")
public class JugadaDto {

	/** The id. */
	private Long id;
	
	/** The partida id. */
	private Long partidaId;
	
	/** The fec recepcion.
	 * Se utiliza el serializer para parsear de manera correcta la fecha 
	 */
	@JsonSerialize(using = DateSerializerUtil.class)
	private ZonedDateTime fecRecepcion;
	
	/** The codigo jugado. */
	private String codigoJugado;
	
	/** The resultado jugado. */
	private String resultadoJugado;
	
	/** The error. */
	private String error;

	/**
	 * Instantiates a new jugada dto.
	 *
	 * @param partidaId the partida id
	 * @param codigoJugado the codigo jugado
	 */
	public JugadaDto(Long partidaId, String codigoJugado) {
		super();
		this.partidaId = partidaId;
		this.codigoJugado = codigoJugado;
	}

	/**
	 * Instantiates a new jugada dto.
	 */
	public JugadaDto() {
		super();
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@Id
	@Column(name = "id_jugada")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the fec recepcion.
	 *
	 * @return the fec recepcion
	 */
	@Column(name = "fec_recepcion", insertable = false, updatable = false)
	public ZonedDateTime getFecRecepcion() {
		return fecRecepcion;
	}

	/**
	 * Sets the fec recepcion.
	 *
	 * @param fecRecepcion the new fec recepcion
	 */
	public void setFecRecepcion(ZonedDateTime fecRecepcion) {
		this.fecRecepcion = fecRecepcion;
	}

	/**
	 * Gets the codigo jugado.
	 *
	 * @return the codigo jugado
	 */
	@Column(name = "codigo_jugado")
	public String getCodigoJugado() {
		return codigoJugado;
	}

	/**
	 * Sets the codigo jugado.
	 *
	 * @param codigoJugado the new codigo jugado
	 */
	public void setCodigoJugado(String codigoJugado) {
		this.codigoJugado = codigoJugado;
	}

	/**
	 * Gets the resultado jugado.
	 *
	 * @return the resultado jugado
	 */
	@Column(name = "resultado_jugado")
	public String getResultadoJugado() {
		return resultadoJugado;
	}

	/**
	 * Sets the resultado jugado.
	 *
	 * @param resultadoJugado the new resultado jugado
	 */
	public void setResultadoJugado(String resultadoJugado) {
		this.resultadoJugado = resultadoJugado;
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * Sets the error.
	 *
	 * @param error the new error
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * Gets the partida id.
	 *
	 * @return the partida id
	 */
	@Column(name = "id_partida")
	public Long getPartidaId() {
		return partidaId;
	}

	/**
	 * Sets the partida id.
	 *
	 * @param partidaId the new partida id
	 */
	public void setPartidaId(Long partidaId) {
		this.partidaId = partidaId;
	}
}
