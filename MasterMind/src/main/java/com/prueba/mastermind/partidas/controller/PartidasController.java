package com.prueba.mastermind.partidas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.mastermind.exceptions.ServiceException;
import com.prueba.mastermind.partidas.dto.PartidaDto;
import com.prueba.mastermind.partidas.dto.PartidaResponseDto;
import com.prueba.mastermind.partidas.servicio.PartidasService;
import com.prueba.mastermind.utils.HttpResponsesPartidasUtil;

/**
 * The Class PartidasController.
 */
@RestController
@RequestMapping(value = "/partidas", produces = "application/json")
@CrossOrigin(origins = "http://localhost:4200")
public class PartidasController {
		
	/** The servicio partidas. */
	@Autowired
	private PartidasService servicioPartidas;

	/**
	 * Comenzar partida.
	 *
	 * @param name the name
	 * @return the response entity
	 */
	@RequestMapping(value = {"/comenzarPartida"}, method = RequestMethod.POST)
	public ResponseEntity<PartidaResponseDto> comenzarPartida(@RequestBody String name) {
		try {
			PartidaDto partidaNueva = servicioPartidas.comenzarPartida(name);
			return HttpResponsesPartidasUtil.encapsularRespuestaSimple(partidaNueva, partidaNueva.getError());
		} catch(ServiceException se) {
			return HttpResponsesPartidasUtil.encapsularRespuestaSimple(null, se.getMessage());
		}
	}

	/**
	 * Gets the partidas.
	 *
	 * @return the partidas
	 */
	@RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
	public ResponseEntity<PartidaResponseDto> getPartidas() {
		try {
			List<PartidaDto> listPartidas = servicioPartidas.obtenerPartidas();
			return HttpResponsesPartidasUtil.encapsularRespuestaLista(listPartidas, null);
		} catch(ServiceException se) {
			return HttpResponsesPartidasUtil.encapsularRespuestaSimple(null, se.getMessage());
		}
	}
	
	/**
	 * Gets the partida.
	 *
	 * @param id the id
	 * @return the partida
	 */
	@RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
	public ResponseEntity<PartidaResponseDto> getPartida(@PathVariable Long id) {
		try {
			PartidaDto partidaExistente = servicioPartidas.obtenerPartida(id);
			return HttpResponsesPartidasUtil.encapsularRespuestaSimple(partidaExistente, null);
		} catch(ServiceException se) {
			return HttpResponsesPartidasUtil.encapsularRespuestaSimple(null, se.getMessage());
		}
	}
}
