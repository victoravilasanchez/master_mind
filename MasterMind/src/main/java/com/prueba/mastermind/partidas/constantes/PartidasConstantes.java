package com.prueba.mastermind.partidas.constantes;

import java.util.ArrayList;
import java.util.List;


/**
 * The Class PartidasConstantes.
 */
public class PartidasConstantes {
	
	/** The Constant GAME_STATUS_STARTED. */
	public static final Integer GAME_STATUS_STARTED = 0;
	
	/** The Constant GAME_STATUS_ERROR. */
	public static final Integer GAME_STATUS_ERROR = -1;
	
	/** The Constant GAME_STATUS_ENDED_WIN. */
	public static final Integer GAME_STATUS_ENDED_WIN = 1;
	
	/** The Constant GAME_STATUS_ENDED_LOSE. */
	public static final Integer GAME_STATUS_ENDED_LOSE = 2;
	
	/** The Constant COLOR_RED. */
	public static final String COLOR_RED = "R";
	
	/** The Constant COLOR_GREEN. */
	public static final String COLOR_GREEN = "G";
	
	/** The Constant COLOR_BLUE. */
	public static final String COLOR_BLUE = "B";
	
	/** The Constant COLOR_YELLOW. */
	public static final String COLOR_YELLOW = "Y";
	
	/** The Constant COLOR_PINK. */
	public static final String COLOR_PINK = "P";
	
	/** The Constant COLOR_ORANGE. */
	public static final String COLOR_ORANGE = "O";
	
	/** The Constant COLOR_ACIERTO_TOTAL. */
	public static final String COLOR_ACIERTO_TOTAL = "W";
	
	/** The Constant COLOR_ACIERTO. */
	public static final String COLOR_ACIERTO = "X";
	
	/** The Constant COLOR_CORRECTO. */
	public static final String COLOR_CORRECTO = "W,W,W,W";
	
	/** The Constant resultados. */
	public static final List<String> resultados = new ArrayList<String>() {
		private static final long serialVersionUID = -9053921899281243188L;

		{
			add(COLOR_RED);
			add(COLOR_GREEN);
			add(COLOR_BLUE);
			add(COLOR_YELLOW);
			add(COLOR_PINK);
			add(COLOR_ORANGE);
		}
	};
}
