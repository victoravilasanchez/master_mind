package com.prueba.mastermind.jugadas.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class JugadaResponseDto.
 */
public class JugadaResponseDto {
	
	/** The lst jugadas. */
	private List<JugadaDto> lstJugadas = new ArrayList<JugadaDto>();
	
	/** The texto error. */
	private String textoError;	

	/**
	 * Instantiates a new jugada response dto.
	 *
	 * @param lstJugadas the lst jugadas
	 * @param textoError the texto error
	 */
	public JugadaResponseDto(List<JugadaDto> lstJugadas, String textoError) {
		super();
		this.lstJugadas = lstJugadas;
		this.textoError = textoError;
	}

	/**
	 * Instantiates a new jugada response dto.
	 *
	 * @param lstJugadas the lst jugadas
	 */
	public JugadaResponseDto(List<JugadaDto> lstJugadas) {
		super();
		this.lstJugadas = lstJugadas;
	}

	/**
	 * Instantiates a new jugada response dto.
	 *
	 * @param textoError the texto error
	 */
	public JugadaResponseDto(String textoError) {
		super();
		this.textoError = textoError;
	}
	
	/**
	 * Instantiates a new jugada response dto.
	 *
	 * @param jugada the jugada
	 */
	public JugadaResponseDto(JugadaDto jugada) {
		super();
		this.lstJugadas.add(jugada);
	}

	/**
	 * Instantiates a new jugada response dto.
	 */
	public JugadaResponseDto() {
		super();
	}

	/**
	 * Gets the lst jugadas.
	 *
	 * @return the lst jugadas
	 */
	public List<JugadaDto> getLstJugadas() {
		return lstJugadas;
	}

	/**
	 * Sets the lst jugadas.
	 *
	 * @param lstJugadas the new lst jugadas
	 */
	public void setLstJugadas(List<JugadaDto> lstJugadas) {
		this.lstJugadas = lstJugadas;
	}

	/**
	 * Gets the texto error.
	 *
	 * @return the texto error
	 */
	public String getTextoError() {
		return textoError;
	}

	/**
	 * Sets the texto error.
	 *
	 * @param textoError the new texto error
	 */
	public void setTextoError(String textoError) {
		this.textoError = textoError;
	}
}
