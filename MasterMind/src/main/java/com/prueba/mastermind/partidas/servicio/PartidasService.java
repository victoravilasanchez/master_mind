package com.prueba.mastermind.partidas.servicio;

import java.util.List;

import com.prueba.mastermind.exceptions.ServiceException;
import com.prueba.mastermind.partidas.dto.PartidaDto;

/**
 * The Interface PartidasService.
 */
public interface PartidasService {

	/**
	 * Obtener partidas.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PartidaDto> obtenerPartidas() throws ServiceException;
	
	/**
	 * Obtener partida.
	 *
	 * @param id the id
	 * @return the partida dto
	 * @throws ServiceException the service exception
	 */
	public PartidaDto obtenerPartida(Long id) throws ServiceException;
	
	/**
	 * Comenzar partida.
	 *
	 * @param name the name
	 * @return the partida dto
	 * @throws ServiceException the service exception
	 */
	public PartidaDto comenzarPartida(String name) throws ServiceException;
	
}
