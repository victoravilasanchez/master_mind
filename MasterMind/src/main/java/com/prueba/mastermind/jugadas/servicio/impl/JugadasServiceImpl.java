package com.prueba.mastermind.jugadas.servicio.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import com.prueba.mastermind.conf.HibernateUtil;
import com.prueba.mastermind.exceptions.ServiceException;
import com.prueba.mastermind.jugadas.dto.JugadaDto;
import com.prueba.mastermind.jugadas.servicio.JugadasService;
import com.prueba.mastermind.partidas.constantes.PartidasConstantes;
import com.prueba.mastermind.partidas.dto.PartidaDto;

/**
 * The Class JugadasServiceImpl.
 */
@Service("JugadasService")
public class JugadasServiceImpl implements JugadasService{

	/* (non-Javadoc)
	 * @see com.prueba.mastermind.jugadas.servicio.JugadasService#obtenerJugadas(java.lang.Long)
	 */
	@Override
	public List<JugadaDto> obtenerJugadas(Long id) throws ServiceException {		
		return HibernateUtil.selectFromWithID(JugadaDto.class, HibernateUtil.getSessionFactory().openSession(), id);
	}

	/* (non-Javadoc)
	 * @see com.prueba.mastermind.jugadas.servicio.JugadasService#obtenerJugada(java.lang.Long)
	 */
	@Override
	public JugadaDto obtenerJugada(Long id) throws ServiceException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		JugadaDto jugadaDto = session.get(JugadaDto.class, id);
		
		return jugadaDto;
	}
	
	/* (non-Javadoc)
	 * @see com.prueba.mastermind.jugadas.servicio.JugadasService#jugar(com.prueba.mastermind.jugadas.dto.JugadaDto)
	 */
	@Override
	public JugadaDto jugar(JugadaDto jugada) throws ServiceException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		PartidaDto partidaDto = session.get(PartidaDto.class, jugada.getPartidaId());
		
		/*
		 * Caso partida abierta
		 */
		if(partidaDto.getEstado() != PartidasConstantes.GAME_STATUS_ENDED_WIN && 
			partidaDto.getEstado() != PartidasConstantes.GAME_STATUS_ENDED_LOSE) {
			String resultado = obtenerResultado(jugada, partidaDto);
			jugada.setResultadoJugado(resultado);
			
			session.save(jugada);			
			session.save(partidaDto);
			
			session.getTransaction().commit();
			
			session.close();
			
		} else {
			jugada.setError("Partida finalizada. No se puede jugar mas");
		}
		return jugada;
	}

	/**
	 * Obtener resultado.
	 *
	 * @param jugada the jugada
	 * @param partidaDto the partida dto
	 * @return the string
	 */
	private String obtenerResultado(JugadaDto jugada, PartidaDto partidaDto) {
		String[] coloresCorrectos = partidaDto.getCodigoCorrecto().split(",");
		String[] coloresJugados = jugada.getCodigoJugado().split(",");
		List<String> respuesta = new ArrayList<>();
		for(int i = 0; i < coloresJugados.length; i++) {
			for(int j = 0; j < coloresCorrectos.length; j ++) {
				if(coloresJugados[i].equalsIgnoreCase(coloresCorrectos[j])) {
					if(i == j) {
						respuesta.add(PartidasConstantes.COLOR_ACIERTO_TOTAL);
					} else {
						respuesta.add(PartidasConstantes.COLOR_ACIERTO);
					}
				}
			}
		}
		
		String codigoRespuesta = StringUtils.join(respuesta,",");
		/*
		 * Comprobar si es la respuesta correcta
		 */
		partidaDto.setIntentosRestantes(partidaDto.getIntentosRestantes() - 1);
		if(codigoRespuesta.equalsIgnoreCase(PartidasConstantes.COLOR_CORRECTO)) {
			partidaDto.setEstado(PartidasConstantes.GAME_STATUS_ENDED_WIN);
		} else if(partidaDto.getIntentosRestantes() == 0) {
				partidaDto.setEstado(PartidasConstantes.GAME_STATUS_ENDED_LOSE);
		}
		
		return codigoRespuesta;
	}

}
