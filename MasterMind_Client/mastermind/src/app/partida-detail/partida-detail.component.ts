import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Partida } from '../home/home.component';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';
import { JugadasService } from '../jugadas.service';
import { PartidasService } from '../partidas.service';

const colorMap = {
  'O': 'orange',
  'R': 'red',
  'G': 'green',
  'B': 'blue',
  'Y': 'yellow',
  'P': 'pink',
  'W': 'white',
  'X': 'black'
};

export interface Codigo {
  p1: string;
  p2: string;
  p3: string;
  p4: string;
}

export interface Jugada {
  id: number;
  jugada: Codigo;
  respuesta: String[];
}

@Component({
  selector: 'app-partida-detail',
  templateUrl: './partida-detail.component.html',
  styleUrls: ['./partida-detail.component.css']
})
export class PartidaDetailComponent implements OnInit {

  partida: any;
  jugadas: Jugada[] = [];

  constructor(private route: ActivatedRoute, public dialog: MatDialog,
    public partidasService: PartidasService, public jugadasService: JugadasService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.partidasService.getGame(params['id']).subscribe((game: any) => {
        this.partida = game[0];
        this.jugadasService.getJugadasFromPartida(this.partida.id).subscribe((data: any) =>
          this.jugadas = data
      );
      });
    });
  }

  openJugadaDialog() {
    const dialogRef = this.dialog.open(JugadaDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.jugadasService.jugar(result, this.partida.id).subscribe(
          () => this.jugadasService.getJugadasFromPartida(this.partida.id).subscribe(
          (data: any) => {
            this.jugadas = data;
            this.partidasService.getGame(this.partida.id).subscribe((game: any) => {
              this.partida = game[0];
            });
          })
        );
      }
    });
  }

}

@Component({
  selector: 'app-jugada-dialog',
  templateUrl: './jugada.dialog.component.html',
})
export class JugadaDialogComponent {

  jugada: string;

  constructor() {
  }

  inputChanged(event: any) {
    const valores = event.target.value.split(',');
    if (valores.length === 4) {
      let valido = true;
      valores.forEach(function(element) {
        if (!colorMap[element]) {
          valido = false;
        }
      });
      if (valido) {
      this.jugada = valores[0] + ',' + valores[1] + ',' + valores[2] + ',' + valores[3];
      }
    } else {
      this.jugada = null;
    }
  }
}
