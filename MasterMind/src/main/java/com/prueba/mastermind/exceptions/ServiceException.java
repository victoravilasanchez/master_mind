package com.prueba.mastermind.exceptions;

/**
 * The Class ServiceException.
 * No es de buenas practicas devolver la Excepcion generica por lo que creo esta excepcion.
 * En principio no voy a sobreescribir ningun metodo pero si fuera necesario podemos modificar
 * el tratamiento de las excepciones.
 */
public class ServiceException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9057652027877868780L;

}
