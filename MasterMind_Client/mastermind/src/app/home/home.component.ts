import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PartidasService } from '../partidas.service';
import { MatDialog } from '@angular/material/dialog';

export interface Partida {
  name: string;
  id: number;
  estado: number;
  intentos: number;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  myControl = new FormControl();
  options: Partida[];
  filteredOptions: Observable<Partida[]>;

  constructor(private router: Router, private partidasService: PartidasService, public dialog: MatDialog) { }

  ngOnInit() {
    this.partidasService.getAllGames().subscribe((data: any) => {
      this.options = data;
      this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    });
  }

  openDetail(p: Partida) {
    this.router.navigate(['partidaDetail', p.id]);
  }

  openPartidaDialog() {
    const dialogRef = this.dialog.open(PartidaDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.newGame(result);
      }
    });
  }

  newGame(name: string) {
    this.partidasService.startGame(name).subscribe((data: any) => {
      console.log(data[0]);
      this.openDetail(data[0]);
    });
  }

  private _filter(value: string): Partida[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.name.toLowerCase().includes(filterValue));
  }

}

@Component({
  selector: 'app-partida-dialog',
  templateUrl: './partida.nueva.component.html',
})
export class PartidaDialogComponent {

  nombre: string;

  constructor() {
  }

  inputChanged(event: any) {
    this.nombre = event.target.value;
  }
}
