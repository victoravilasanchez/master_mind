package com.prueba.mastermind.conf;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * The Class ApplicationConfiguration.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.prueba.mastermind")
public class ApplicationConfiguration {
	
}